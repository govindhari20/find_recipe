class data_list {
  String recipies;
  String imageLink;
  String description;
  String nutritions;
  String by;
  String recipeID;
  String requiredTime;
  String servings;
  String ingredients;
  String directions;
  String energy;
  String protein;
  String carbs;
  String fiber;
  String fat;
  String cholestrol;
  String sodium;

  data_list({
    this.recipies,
    this.imageLink,
    this.recipeID,
    this.description,
    this.nutritions,
    this.by,
    this.requiredTime,
    this.servings,
    this.ingredients,
    this.directions,
    this.energy,
    this.protein,
    this.carbs,
    this.fiber,
    this.fat,
    this.cholestrol,
    this.sodium,
  });
}
