import 'dart:ui';
import 'package:find_recipe/extension.dart';
import 'package:find_recipe/bottom_screens/directions.dart';
import 'package:find_recipe/bottom_screens/ingredients.dart';
import 'package:find_recipe/bottom_screens/nutrations.dart';
import 'package:flutter/material.dart';
import 'package:find_recipe/custome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:find_recipe/responses.dart';

class recipe_info extends StatefulWidget {
  String url;
  String RecipeID;
  String tag;
  String descrip;
  String title;
  String by;
  String ingredients;
  String time;
  String serve;
  String direct;
  String energy;
  //
  String protein;
  String carbs;
  String fiber;
  String fat;
  //
  String cholestrol;
  String sodium;
  recipe_info(
      {Key key,
      @required this.url,
      @required this.descrip,
      @required this.title,
      @required this.by,
      @required this.ingredients,
      @required this.time,
      @required this.serve,
      @required this.direct,
      @required this.energy,
      @required this.protein,
      @required this.carbs,
      @required this.fiber,
      @required this.fat,
      @required this.cholestrol,
      @required this.sodium,
      @required this.RecipeID})
      : super(key: key);
  @override
  _recipe_infoState createState() => _recipe_infoState(
      this.url,
      this.descrip,
      this.title,
      this.by,
      this.ingredients,
      this.time,
      this.serve,
      this.direct,
      this.energy,
      this.protein,
      this.carbs,
      this.fiber,
      this.fat,
      this.cholestrol,
      this.sodium,
      this.RecipeID);
}

class _recipe_infoState extends State<recipe_info> {
  String url;
  String tag;
  String descript;
  String title;
  String by;
  String ingredient;
  String time;
  String serve;
  String direct;
  String energy;

  //
  String protein;
  String carbs;
  String fiber;
  String fat;
  //
  String cholestrol;
  String sodium;
  String recipeId;

  _recipe_infoState(
      this.url,
      this.descript,
      this.title,
      this.by,
      this.ingredient,
      this.time,
      this.serve,
      this.direct,
      this.energy,
      this.protein,
      this.carbs,
      this.fiber,
      this.fat,
      this.cholestrol,
      this.sodium,
      this.recipeId);
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  var get_title;
  List<Widget> _widgetOptions = List<Widget>();
  @override
  void initState() {
    _widgetOptions.add(ingredients(
      ingredient: ingredient,
      time: time,
      serve: serve,
    ));
    print(serve);
    _widgetOptions.add(directions(
      direct: direct,
      time: time,
      serve: serve,
    ));
    _widgetOptions.add(nutration(
      energy: energy,
      time: time,
      serve: serve,
      protein: protein,
      carbs: carbs,
      fiber: fiber,
      fat: fat,
      cholestrol: cholestrol,
      sodium: sodium,
    ));
    get_title = title.capitalize();
    super.initState();
    getId();
    // itrate();
  }

  bool isgood = false;
  bool isbad = false;
  String id;
  getId() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      id = pref.getString("id");
    });
    var res = await get_goodList(id, recipeId);
    setState(() {
      isgood = res["isGood"];
      isbad = res["isBad"];
    });
    print(res);
  }

  List list = ["1 cup boiled cubes"];

  // itrate() {
  //   list.where((element) {
  //     print("valeu ${element}");
  //     return element.contains(ingredient);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
                tag: "imageHero",
                child: Stack(
                  children: [
                    Container(
                      height: height * 0.5,
                      width: width,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(url), fit: BoxFit.fill)),
                      // child: BackdropFilter(
                      //   filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                      // child: Container(
                      //   height: 0,
                      //   decoration: new BoxDecoration(
                      //       color: Colors.black.withOpacity(0.0)),
                      // ),
                    ),
                    // Positioned.fill(
                    //     child: Align(
                    //         alignment: Alignment.bottomLeft,
                    //         child: frosted(Text("asda")))),
                    Positioned.fill(
                      child: Align(
                          alignment: Alignment.bottomLeft,
                          child: frosted(Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5, top: 5),
                                child: Text(
                                  get_title,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5, bottom: 5),
                                child: Text(
                                  by,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              isgood == false && isbad == false
                                  ? Container()
                                  : isgood == true
                                      ? Container(
                                          height: 50,
                                          width: width,
                                          color: Colors.greenAccent,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 15, top: 8, bottom: 8),
                                            child: Text(
                                              "NOTE: According to you health condition this recipe contains some ingredients which are good for you.",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w800),
                                            ),
                                          ),
                                        )
                                      : isbad == true
                                          ? Container(
                                              height: 50,
                                              width: width,
                                              color: Color(0xFFFFD2D2),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15,
                                                    top: 8,
                                                    bottom: 8),
                                                child: Text(
                                                  "NOTE: According to you health condition this recipe contains some ingredients which are not good for you.",
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w800),
                                                ),
                                              ),
                                            )
                                          : Container()
                            ],
                          ))),
                    )
                  ],
                )),
            IndexedStack(
              index: _selectedIndex,
              children: _widgetOptions,
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.insights),
            label: 'Ingredients',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.description),
            label: 'Directions',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pages),
            label: 'Nutration',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onItemTapped,
      ),
    );
  }

  Widget frosted(Widget child) {
    return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
      ClipRect(
        child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration:
                  new BoxDecoration(color: Colors.black.withOpacity(0.5)),
              child: child,
            )),
      ),
    ]);
  }
}
