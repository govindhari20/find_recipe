import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:find_recipe/recipe_info.dart';
import 'package:find_recipe/responses.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class based_time extends StatefulWidget {
  String id;
  based_time({Key key, @required this.id}) : super(key: key);
  @override
  _based_timeState createState() => _based_timeState(this.id);
}

class _based_timeState extends State<based_time> {
  String id;
  _based_timeState(this.id);
  bool isoffline;
  Future _future;

  SharedPreferences mypref;
  Future<void> connectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      setState(() {
        isoffline = true;
      });

      print("connected");
    } else if (connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        isoffline = true;
      });
      // I am connected to a wifi network.
      print("connected");
    } else {
      setState(() {
        isoffline = false;
      });
      print("dissconneted");
    }
  }

  static const Color one = Color(0xff808000);
  static const Color two = Color(0xff608000);
  static const Color three = Color(0xff208080);
  List<Color> colors = [one, two, three];
  static final random = new Random();

  Color colorrandom() {
    return colors[random.nextInt(3)];
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    connectivity();
    var now = DateTime.now();
    var time = DateFormat('HH').format(now);
    _future = time_based(time.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Based on time"),
      ),
      body: Column(children: [
        Expanded(
            child: isoffline != false
                ? FutureBuilder(
                    future: _future,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return new GridView.builder(
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 5,
                          ),
                          physics: BouncingScrollPhysics(),
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 5,
                              shadowColor: Colors.grey[400],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: InkWell(
                                onTap: () async {
                                  mypref =
                                      await SharedPreferences.getInstance();
                                  mypref.setString(
                                      "title", snapshot.data[index].recipies);
                                  var response = await post_history_data(
                                      snapshot.data[index].recipeID, id);

                                  if (response['response'] == true) {
                                    print("success");
                                  }
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => recipe_info(
                                                RecipeID: snapshot
                                                    .data[index].recipeID
                                                    .toString(),
                                                url: snapshot
                                                    .data[index].imageLink,
                                                descrip: snapshot
                                                    .data[index].description,
                                                title: snapshot
                                                    .data[index].recipies,
                                                by: snapshot.data[index].by,
                                                ingredients: snapshot
                                                    .data[index].ingredients,
                                                time: snapshot
                                                    .data[index].requiredTime
                                                    .toString(),
                                                serve: snapshot
                                                    .data[index].servings
                                                    .toString(),
                                                direct: snapshot
                                                    .data[index].directions,
                                                energy: snapshot.data[index]
                                                    .nutritions.energy,
                                                protein: snapshot.data[index]
                                                    .nutritions.protein,
                                                carbs: snapshot.data[index]
                                                    .nutritions.carbs,
                                                fiber: snapshot.data[index]
                                                    .nutritions.fiber,
                                                fat: snapshot
                                                    .data[index].nutritions.fat,
                                                cholestrol: snapshot.data[index]
                                                    .nutritions.cholestrol,
                                                sodium: snapshot.data[index]
                                                    .nutritions.sodium,
                                              )));
                                },
                                child: Container(
                                  width: 150,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      //mostviewed
                                      ClipRRect(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(8),
                                              topRight: Radius.circular(8)),
                                          child: Container(
                                            height: 100,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            color: colorrandom(),
                                            child: CachedNetworkImage(
                                              imageUrl: snapshot
                                                  .data[index].imageLink,
                                              fit: BoxFit.fill,
                                            ),
                                          )),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Flexible(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            snapshot.data[index].recipies,
                                            maxLines: 2,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 5),
                                        child: Text(
                                          "By ${snapshot.data[index].by}",
                                          style: TextStyle(
                                              color: Colors.grey[700]),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),

                                      Padding(
                                        padding: EdgeInsets.only(left: 5),
                                        child: Text(
                                          "Cooking Time: ${snapshot.data[index].requiredTime} min",
                                          style: TextStyle(
                                              color: Colors.grey[700]),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                          child: Text("error"),
                        );
                      }
                    })
                : Center(child: Text("No internet"))),
      ]),
    );
  }
}
