import 'package:flutter/material.dart';

class directions extends StatefulWidget {
  String direct;
  String time;
  String serve;
  directions(
      {Key key,
      @required this.direct,
      @required this.time,
      @required this.serve})
      : super(key: key);
  @override
  _directionsState createState() =>
      _directionsState(this.direct, this.time, this.serve);
}

class _directionsState extends State<directions> {
  String direct;
  String time;
  String serve;
  _directionsState(this.direct, this.time, this.serve);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text("Directions",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  )),
              Text("Ready In: ${time}min",
                  style: TextStyle(
                    fontSize: 16,
                    // fontWeight: FontWeight.bold,
                  )),
              Text("Serves: ${serve}",
                  style: TextStyle(
                    fontSize: 16,
                    // fontWeight: FontWeight.bold,
                  ))
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              direct,
              style: TextStyle(fontSize: 16),
            ))
      ],
    );
  }
}
