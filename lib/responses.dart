import 'dart:async';
import 'dart:convert';

import 'package:find_recipe/api.dart';
import 'package:find_recipe/bottom_screens/directions.dart';
import 'package:find_recipe/bottom_screens/nutrations.dart';
import 'package:find_recipe/json_data.dart';
import 'package:find_recipe/json_parse_data/json_pojo.dart';
import 'package:find_recipe/my_jsons/recipe_json_data.dart';

import 'package:http/http.dart' as http;

Future login_response(email, password) async {
  var url = "http://10.0.2.2:5000/login2";
  var response = await http.post(url,
      headers: {"Accept": "application/json"},
      body: {"mail": email, "pass": password});

  var json_res = jsonDecode(response.body);

  return json_res;
}

Future signup_response(email, password) async {
  var url = "http://10.0.2.2:5000/";

  var response = await http.post(url,
      headers: {"Accept": "application/json"},
      body: {"email": email, "password": password});

  var json_res = jsonDecode(response.body);

  return json_res;
}

Future signup2(user_name, age, gender, disease, vegetarian, id) async {
  final response = await http.post("http://10.0.2.2:5000/signup2", headers: {
    "Accept": "application/json"
  }, body: {
    "id": id,
    "username": user_name,
    "age": age,
    "gender": gender,
    "disease": disease,
    "vegetarian": vegetarian
  });

  var json_res = json.decode(response.body);
  return json_res;
}

Future signup3(String id, prefernce) async {
  var response;
  String list = json.encoder.convert(prefernce);

  response = await http.post("http://10.0.2.2:5000/signup3",
      headers: {"Accept": "application/json"},
      body: {"id": id, "preference": list});

  var json_res = jsonDecode(response.body);
  return json_res;
}

Future<List<Recipies>> data() async {
  try {
    var response_data = await http.get("http://10.0.2.2:5000/alldata");

    if (response_data.statusCode == 200) {
      return recipiesFromJson(response_data.body);
    }
  } catch (e) {
    throw Exception(e.toString());
  }
}

// Future<List<JsonData>> data2() async {
//   try {
//     var response_data =
//         await http.get("https://webrooper.com/androiddb/foodlist.php");

//     if (response_data.statusCode == 200) {
//       return jsonDataFromJson(response_data.body);
//     }
//   } catch (e) {
//     throw Exception(e.toString());
//   }
// }

// initialization
Future<List<Recipies>> get_init_recipies(id) async {
  try {
    var response =
        await http.post("http://10.0.2.2:5000/get_reccoom", body: {"id": id});
    print(response.body.length);
    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

Future<List<data_list>> getmoredata() async {
  List<data_list> mylist = [];
  try {
    var response = await http.get("http://10.0.2.2:5000/alldata");

    var json_ob = jsonDecode(response.body);

    for (var u in json_ob) {
      data_list data = data_list(
          recipeID: u['RecipeID'].toString(),
          recipies: u['recipies'],
          imageLink: u['ImageLink'],
          description: u['Description'],
          by: u['By'],
          requiredTime: u['RequiredTime'].toString(),
          servings: u['Servings'].toString(),
          ingredients: u['Ingredients'],
          directions: u['Directions'],
          energy: u['Nutritions']['energy'],
          protein: u['Nutritions']['protein'],
          carbs: u['Nutritions']['carbs'],
          fiber: u['Nutritions']['fiber'],
          fat: u['Nutritions']['fat'],
          cholestrol: u['Nutritions']['cholestrol'],
          sodium: u['Nutritions']['sodium']);
      mylist.add(data);
    }

    return mylist;
  } catch (e) {
    throw e;
  }
}

Future<List<Recipies>> auto_scroll(id) async {
  try {
    var response =
        await http.post("http://10.0.2.2:5000/auto_scroll", body: {"id": id});
    print("auto_scroll res ${response.body}");
    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

Future<List<filter>> getmoredata2() async {
  List<filter> mylist = [];
  try {
    var response =
        await http.get("https://webrooper.com/androiddb/foodlist.php");

    var json_ob = jsonDecode(response.body);

    for (var u in json_ob) {
      filter data = filter(u['title'], u['imgurl']);
      mylist.add(data);
    }

    return mylist;
  } catch (e) {
    return e;
  }
}

class filter {
  String productId;
  String imgurl;
  String title;
  String price;

  filter(this.title, this.imgurl);
}

Future<List<Recipies>> recommended_recipe(title) async {
  try {
    var response =
        await http.post("http://10.0.2.2:5000/recomm", body: {"title": title});

    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

/// history related apis

Future post_history_data(id, iid) async {
  var response = await http.post("http://10.0.2.2:5000/history",
      body: {"id": id.toString(), "user_id": iid});
  var json_body = jsonDecode(response.body);
  return json_body;
}

Future<List<Recipies>> show_history(id) async {
  try {
    var response =
        await http.post("http://10.0.2.2:5000/show_history", body: {"id": id});

    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

Future<List<Recipies>> ingredient_recipe(ing_list, id) async {
  String list = json.encoder.convert(ing_list);
  try {
    var response = await http.post("http://10.0.2.2:5000/ing_list",
        body: {"ing_list": list, "id": id});
    print(response.body);
    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

Future<List<Recipies>> time_based(time) async {
  try {
    var response = await http
        .post("http://10.0.2.2:5000/time_based", body: {"time": time});

    print(response.body);
    return recipiesFromJson(response.body);
  } catch (e) {
    throw Exception(e);
  }
}

Future get_pref(id) async {
  List _list = [];
  String value = "";
  try {
    var response =
        await http.post("http://10.0.2.2:5000/pref", body: {"id": id});
    var result = json.decode(response.body);
    for (var i in result) {
      value = value + " " + i;
    }
    return value;
  } catch (e) {
    return e;
  }
}

Future post_disease(user_id, disease) async {
  try {
    final result = await http.post("http://10.0.2.2:5000/disease",
        body: {"id": user_id, "disease": disease});

    return result.body;
  } catch (e) {
    return e;
  }
}

Future get_goodList(user_id, recipe_id) async {
  final result = await http.post("http://10.0.2.2:5000/healthy",
      body: {"recipe_id": recipe_id, "id": user_id});
  var response = json.decode(result.body);
  return response;
}
